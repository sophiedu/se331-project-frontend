import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { CourseListComponent } from './course/list/list.component';
import { CourseAddComponent } from './course/add/add.component';
import { LoginComponent } from './login/login.component';
import { StudentsViewComponent } from './students/view/students.view.component';
import { LecturerGuard } from './lecturer-guard.guard';


const appRoutes: Routes = [
  
  // {
  //   path: '',
  //   redirectTo: '/list',
  //   pathMatch: 'full'
  // },
  {
      path: '',
      redirectTo: '/login',
      pathMatch: 'full'
    },
  {
    path: 'course/list',
    component: CourseListComponent,
    canActivate: [LecturerGuard]
  },
  {
    path: 'course/add',
    component: CourseAddComponent,
    canActivate: [LecturerGuard]
  },
  {
    path: 'students/list',
    component: StudentsViewComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  
  { path: '**', component: FileNotFoundComponent }

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
